package br.com.farmacia.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Cidade extends GenericDomain {
    
    @ManyToOne
    @JoinColumn(name = "codigo_estado", nullable = false)
    private Estado estado;
	
    @Column(length = 80, nullable = false)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Estado getEstado() {
    	return estado;
    }
    
    public void setEstado(Estado estado) {
    	this.estado = estado;
    }
}