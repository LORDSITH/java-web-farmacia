package br.com.farmacia.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@SuppressWarnings("serial")
@Entity
public class ItemVenda extends GenericDomain {

	@Column(nullable = false)
	private Short quantidade;
	
	@Column(nullable = false, precision = 6, scale = 2)
	private BigDecimal valorParcial;
	
	@ManyToOne
	@JoinColumn(name = "codigo_produto", nullable = false)
	private Produto produto;
	
	@OneToOne
	@JoinColumn(name = "codigo_venda", nullable = false)
	private Venda venda;
	
	public Short getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Short quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getValorParcia() {
		return valorParcial;
	}
	public void setValorParcia(BigDecimal valorParcia) {
		this.valorParcial = valorParcia;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Venda getVenda() {
		return venda;
	}
	public void setVenda(Venda venda) {
		this.venda = venda;
	}
}
