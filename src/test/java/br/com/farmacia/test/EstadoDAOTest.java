package br.com.farmacia.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.farmacia.dao.EstadoDAO;
import br.com.farmacia.domain.Estado;

public class EstadoDAOTest {
	@Test
	public void salvar() {
		Estado estado = new Estado();
		EstadoDAO estadoDAO = new EstadoDAO();
		
		estado.setNome("Gasoso");
		estado.setSigla("GA");
		
		estadoDAO.salvar(estado);
	}
	
	@Test
	@Ignore
	public void listar() {
		
		EstadoDAO estadoDAO = new EstadoDAO();
		List<Estado> resultado = estadoDAO.listar();
		
		System.out.println("Total de Registros encontrados: " + resultado.size());
		
		for (Estado estado : resultado) {
			System.out.println(estado.getNome());
		}
	}
	
	@Test
	public void buscar() {
		Long codigo = 1L;
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = new Estado();
		
		estado = estadoDAO.buscar(codigo);
		System.out.println(estado.getNome());
	}
	
	@Test
	public void excluir() {
		
		Long codigo = 1L;

		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = estadoDAO.buscar(codigo);
				
		if (estado == null) {
			System.out.println("Nenhum Registro Encontrado");
		} else {
			estadoDAO.excluir(estado);
			System.out.println("Registro removido");
			System.out.println(estado.getNome());
		}
	}
	
	@Test
	public void editar() {
		
		Long codigo = 1L;
		
		EstadoDAO estadoDAO = new EstadoDAO();
		Estado estado = estadoDAO.buscar(codigo);
		
		if (estado == null) {
			System.out.println("Nenhum Registro Encontrado");
		} else {
			
			System.out.println("Registro editado - Antes:");
			System.out.println(estado.getCodigo() + " - " + estado.getSigla() + " - " + estado.getNome());
			
			estado.setNome("Líquido");
			estado.setSigla("LI");
			estadoDAO.editar(estado);
			
			System.out.println("Registro editado");
			System.out.println(estado.getCodigo() + " - " + estado.getSigla() + " - " + estado.getNome());
		}
		
	}

}
